# ChangeLog

All notable changes to this project are documented in this file.

## [0.10.2] - 2022-01-24

- Explicitly manage nvidia-gpu-device-plugin tag in chart values

## [0.10.1] - 2022-01-21

- Update nvidia-gpu dep to 0.4.3 (selinux disable)

## [0.10.0] - 2022-01-20

- Add cvmfs-csi dependency
- Add dependency on upstream kube-prometheus-stack
- Disable generation of cinder storage classes

## [0.9.6] - 2022-01-19

- Add CRD generation to the deploy step in gitlab-ci

## [0.9.4] - 2021-11-29

- Disable generating CRDs by default. CRDs are now being extracted from
  sub-charts as a separate step during CI, and are stored in crds/ directory.
- Update dep versions for traefik, csi-cinder, csi-manila

## [0.9.0] - 2021-10-04

- Add traefik V2 installation from umbrella chart

## [0.8.7] - 2021-11-03

- Add snapshot-controller disabled by default
- Add snapshot-validation-webhook disabled by default

## [0.8.6] - 2021-10-26

- Add cert-manager.io disabled by default

## [0.8.5] - 2021-10-26

- Add node-problem-detector 2.0.9 disabled by default

## [0.8.4] - 2021-07-20

- Disable HTTP metrics services in ceph-csi-cephfs

## [0.8.3] - 2021-06-25

- Disable topology in csi-cinder

## [0.8.2] - 2021-06-24

- Update cern-base (hostcert and keytab) to 0.5.3

## [0.8.1] - 2021-06-23

- Update cern-base (hostcert and keytab) to 0.5.2

## [0.8.0] - 2021-03-24

- Add openstack-manila-csi 1.1.1 disabled by default
- Add Manila storage classes for CephFS share types:
   * Geneva CephFS Testing, storage class name geneva-cephfs-testing
   * Meyrin CephFS, storage class name meyrin-cephfs
   * Meyrin CephFS Vault SSD A, storage class name meyrin-cephfs-vault-ssd-a
   * These are enabled only if openstack-manila-csi is enabled.
- Add ceph-csi-cephfs 3.3.1 disabled by default
- Bump openstack-cinder-csi to 1.3.7

## [0.7.0] - 2021-03-02

- Update eosxd helm chart to 0.2.7

## [0.5.0] - 2020-07-10

- Add base 0.1.0 disabled by default  for cern-keytab and cern-hostcert.

## [0.4.0] - 2020-06-26

- Add openstack-cinder-csi 1.1.1 disabled by default

## [0.3.0] - 2020-05-27

- Add requirement on fluentd with a set of default values
- Add requirement on landb-sync
- Add requirement on prometheus-cern

## [0.1.1] - 2020-05-06

- Update nvidia-gpu requirement to 0.3.0
- Fix gitlab-ci with appropriate cern repo

## [0.1.0] - 2020-04-30

- First Release
